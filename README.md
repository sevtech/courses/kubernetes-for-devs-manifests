# kubernetes-for-devs-manifests 

_kubernetes-for-devs-manifests_ is a repository containing examples of manifests of the various kubernetes objects, which a developer may have to configure.


### Requirements 📋

* [Kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/) 
* [OktetoCloud Account](https://okteto.com/) 

---
⌨️ with ❤️ by [franvallano](https://www.linkedin.com/in/francisco-javier-delgado-vallano-b28b1670/)
